# README #
Resolução da PL01 12/03/2021
##Pergunta 1
O Maven é uma ferramenta desenvolvida pela Apache, ela serve para gerenciar as dependências e automatizar seus builds.

##Pergunta 2
O pom.xml é o ficheiro de configuração do maven, onde podemos adicionar dependencias e propriedades.

##Pergunta 3
O DVCS (Distributed Version Control System) é um género de controlador de versões, incluído o seu histórico, é espelhado nos computadores de todos os desenvolvedores.

##Pergunta 4
* Repositório Remoto: é uma cloud que armazena o projeto
* Area de trabalho: zona de trabalho de cada membro da equipa
* Repositório Local: armazenamento do projeto no computador de cada individuo
* Staged (Indexed) Files: Ficheiros que estão prontos para dar commit
* Committed Files: ficheiros prontos a serem enviados para o repositorio remoto
* Clone, Push, Pull = Fetch + Merge: enviar os ficheiros de forma a que fiquem atualizados sem interferir com os commits dos outros membros.

##Pergunta 5
O .gitignore geralmente é usado para evitar a adição de ficheiros irrelevantes (que não são úteis) para outros colaboradores tais como produtos de compilação ou ficheiros temporários criados.
##Pergunta 6
O clone de um repositório é a copia do repositório remoto para o repositório local.

##Pergunta 7
Um fork acontece quando um desenvolvedor inicia um projeto independente com base no código de um projeto já existente, ou seja, quando um software é desenvolvido com base em outro, já existente, sem a descontinuidade deste último.

## Pergunta 8
O utilizador 1 tem uma abordagem do tipo fast forward, pois o seu push não foi interrompido por um push de outro utilizador, no entanto, o utilizador 2 teve o seu push recusado,logo teve uma abordagem non fast foward e necessitou de fazer um fecth+merge sntes de efetuar o commit dele.


